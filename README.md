# Weechat-alert_dunst
Simple hook to use dunst to notify me in my Fedora DWM development station.

## Installation

* Place weechat-alert_dunst.rb in your Weechat config dir under ruby
```
cp weechat-alert_dunst.rb ~/.weechat/ruby/
```

## Use

* Load weechat-alert_dunst.rb in your weechat session
```
  /ruby load weechat-alert_dunst.rb 
```
