# encoding: UTF-8
# ==================================================================================================
#  weechat-alert_dunts.rb (c) May 2013 by Guillermo Gomez S. <guillermo.gomez@gmail.com>
#
#  Licence     : GPL v2
#  Description : Sends alerts to the OS
#  Syntax      : /weechat-alert_dunts
#
# ==================================================================================================

def weechat_init
  Weechat.register('weechat-alert_dunst', 'Guillermo Gomez (gomix) <guillermo.gomez@gmail.com>', '0.1', 'GPL2', 'Sends an alert to the OS', '', '')
  Weechat.hook_signal("irc_pv","weechat-alert_dunst", "")
  Weechat.hook_signal("weechat_highlight","weechat-alert_dunst", "")
  Weechat.print("", "weechat-alert_dunst.rb loaded!")
  return Weechat::WEECHAT_RC_OK
end

def alert_dunst(data, buffer, args)
  Weechat.print("", "args: #{args}")
  # Si args trae PRIVMSG, entonces es un PV
  # Si no, es un resaltado de weechat (usualmente porque mencionan mi nick)
  case args
  when /.*PRIVMSG.*/
    msg = args.delete("PRIVMSG")
    `notify-send -u critical "PV #{msg}"`
    #`play -v 0.05 ~/Sonidos/IA_pause.mp3 -q`
    return Weechat::WEECHAT_RC_OK
  when "B"
    `notify-send -u normal "B #{args}"`
    #`play -v 0.05 ~/Sonidos/IA_pause.mp3 -q`
    return Weechat::WEECHAT_RC_OK
  else
    `notify-send -u normal "#{args}"`
    #`play -v 0.05 ~/Sonidos/IA_pause.mp3 -q`
    return Weechat::WEECHAT_RC_OK
  end
  # TODO:
  # 1. If in a pv chat, active, i should not be notified constantly with dunst
  # ISSUES:
  # 1. Sound playback, pulseaudio/comando play
  #    play FAIL formats: can't open output file `default'
end
